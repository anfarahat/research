% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[10pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options
\usepackage{epstopdf} %support direct compilation of eps into pdf
% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!


\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{algorithm2e}

\usepackage{hyperref}
\usepackage{flashmovie}
\usepackage{media9}

\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}

\usepackage{listings}
\usepackage{physics}

%%% END Article customizations

%%% The "real" document content comes below...

\title{Derivation of Lagrangian Equations of Motions for Nonholonomic Systems}
\author{Aly Farahat\\ \emph{Intuitive Surgical Inc.}}
\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\maketitle

\section{Lagrangian Mechanics}

Lagrangian/Analytical mechanics is a mathematical formalism that enables the systematic derivation of the equations of motion of an arbitrary mechanical system. Unlike the Newtonian formalism, Lagrangian mechanics need not formulate the acceleration vectors of particles in a particular coordinate system -- a rather tedious job. Instead, analytical mechanics needs only to compute the kinetic energy and work function of the whole system of particles. From there, a systematic derivation of the equations of motion takes place, for any coordinate system of interest. 

Furthermore, analytical mechanics could altogether avoid the computation of reaction forces due to motion constraints by a suitable choice of coordinate system/configuration space. In fact, we are only interested in coordinates whose values change given the constraints of motion, thereby solving a potentially reduced number of differential equations of motion that refelect the exact number of degrees of freedom of the mechanical system.

The central result of Lagrangian mechanics is the Lagrange equations of motion of the second kind. The form of the Lagrange equation is coordinate invariant; i.e., it holds true in any coordinate system of choice. In terms of the total kinetic energy $T$ of the mechanical system, we have

\begin{equation}
\frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial T}{\partial \dot{q}^i} - \frac{\partial T}{\partial q^i} = Q_i,
\end{equation}

where $1 \leq i \leq n$, $n$ is the number of degrees of freedom of the mechanical system. $Q_i$ is the generalized component force contravariant to $q^i$.

Since $T$ is an invariant, $Q_i$ is a \emph{covariant} tensor: it transforms from coordinate to the other by multiplying it with $J^{i}_j = \frac{\partial q^i}{\partial x^j}$. 

The derivation of Lagrange equations becomes straightforward if we make use of tensor notation. In Cartesian coordinates, it is easy to see that the equations of motion of an arbitrary system of particles with contraints is

\begin{equation}
\frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial T}{\partial \dot{x}^i} = F_i + R_i
\end{equation}
where $F_i$ is the covariant components of the force and $R_i$ is the covariant component of the reactions imposed by the constraints. To transform to generalized coordinates, we multiply the equations motion by $J^i_j$ and use tensor notation to add all the equations.

\begin{equation}
\frac{\partial x^i}{\partial q^j} \frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial T}{\partial \dot{x}^i} = \frac{\partial x^i}{\partial q^j} F_i + \frac{\partial x^i}{\partial q^j} R_i
\end{equation} 

We have chosen the coordinates $q^i$ such that the generalized constraint reactions vanish along the direction of change of $q^i$ (ideal constraints); thus $R_i \frac{\partial x^i}{\partial q^j} = 0$.

\begin{equation}
\label{eq:EL1}
\frac{\partial x^i}{\partial q^j} \frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial T}{\partial \dot{x}^i} = Q_j
\end{equation}

By looking at the RHS of Equation (\ref{eq:EL1}), we observe that it is of the form $A \frac{\mathrm{d}B}{\mathrm{d}t}$. Using the product rule of differentiation, this can be rewritten as  $\frac{\mathrm{d}(AB)}{\mathrm{d}t} - \frac{\mathrm{d}A}{\mathrm{d}t}B$, thus

\begin{equation}
\label{eq:EL2}
\frac{\mathrm{d}}{\mathrm{d}t}\Big( \frac{\partial x^i}{\partial q^j} \frac{\partial T}{\partial \dot{x}^i} \Big) - \frac{\partial T}{\partial \dot{x}^i} \frac{\mathrm{d}}{\mathrm{d}t} \frac{\partial x^i}{\partial q^j} = Q_j
\end{equation}

Note that $\dot{x}^i = \frac{\partial x^i}{\partial q^j}\dot{q}^j + \frac{\partial x^i}{\partial t}$, from which follows that
\begin{equation}
\label{eq:h1}
\frac{\partial \dot{x}^i}{\partial \dot{q}^j} = \frac{\partial x^i}{\partial q^j}
\end{equation}

Also note that the order of differentation of $x^i$ does not matter, in fact,

\begin{equation}
\label{eq:h21}
\frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial x^i}{\partial q^j} = \frac{\partial^2 x^i}{\partial q^j \partial q^k} \dot{q}^k + \frac{\partial^2 x^i}{\partial q^j \partial \dot{q}^k} \ddot{q}^k + \frac{\partial^2 x^i}{\partial q^j \partial t}
\end{equation}

By switching the order of differentiation in Eqaution (\ref{eq:h21}), we obtain that
\begin{equation}
\label{eq:h2}
\frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial x^i}{\partial q^j} = \frac{\partial}{\partial q^j} \Big( \frac{\partial x^i}{\partial q^k}\dot{q}^k + \frac{\partial x^i}{\partial \dot{q}^k}\ddot{q}^k + \frac{\partial x^i}{\partial t} \Big) = \frac{\partial}{\partial q^j}\frac{\mathrm{d} x^i}{\mathrm{d}t} = \frac{\partial {\dot{x}^i}}{\partial q^j}
\end{equation}
From Equations (\ref{eq:h1}) and (\ref{eq:h2}) into Equation (\ref{eq:EL2}),

\begin{equation}
\frac{\mathrm{d}}{\mathrm{d}t}\Big( \frac{\partial \dot{x}^i}{\partial \dot{q}^j} \frac{\partial T}{\partial \dot{x}^i} \Big) - \frac{\partial \dot{x}^i}{\partial q^j} \frac{\partial T}{\partial \dot{x}^i} = Q_j
\end{equation}

which reduces to Lagrange Equation of the second kind for holonomic systems.


\begin{equation}
\label{eq:LSecond}
\boxed{\Big( \frac{\mathrm{d}}{\mathrm{d}t} \frac{\partial}{\partial \dot{q}^j} - \frac{\partial}{\partial q^j} \Big) T = Q_j}
\end{equation}

\section{Nonholonomic Linear Velocity Constraints and Lagrange Equations}
The introduction of non-integrable constraints having linear velocity terms imposes some conditions to preserve the validity of the Lagrangean formalism.

Unlike holonomic constraints, nonholonomic constraints entail that Cartesian positions depend on the full history of values of pseudo-cooredinates. As such, only Cartesian velocities are linear functions of independent pseudovelocities at the same time instant. Independent pseudovelicities are chosen such that they span the null vector space of the constraint equations. This way, we guarantee that arbitrary choices of virtual pseudo-displacements generate valid corresponding virtual displacements in the Cartesian space.

\begin{equation} \label{Eq:Constraint}
\dot{x}^i = h^i_j \dot{q}^j + h^i_o 
\end{equation}

or equivalently, in virtual displacement form,
\begin{equation}
\delta x^i = h^i_j \delta {q}^j 
\end{equation}

where $h^i_j \equiv \frac{\partial \dot{x}^i}{\partial{\dot{q}^j}}$.

Similar to the derivation for the holonomic case, we express the general equation of dynamics as such.

\begin{equation}
\Big( \frac{d}{dt} \frac{\partial T}{\partial{\dot{x}^i}} - F_i\Big) \delta x^i = 0
\end{equation}
where the ideal reactions of all the constraints have vanished.

Now, we express the Cartesian virtual displacement in terms of the virtual pseudo-displacement,

\begin{equation}
\Big( \big( \frac{d}{dt} \frac{\partial T}{\partial{\dot{x}^i}} - F_i\big) h^i_j \Big) \delta q^j = 0
\end{equation}

given the independence of the virtual pseudo-displacements and using the identity for $h^i_j$,

\begin{equation}
\forall j, \quad \frac{\partial \dot{x}^i}{\partial \dot{q}^j} \Big(\frac{d}{dt} \frac{\partial T}{\partial{\dot{x}^i}} \Big)  = F_i \frac{\partial \dot{x}^i}{\partial \dot{q}^j} = Q_j
\end{equation}

Again, applying the product rule similar to the holonomic case,

\begin{equation} \label{Eq:LNO}
\frac{d}{dt}\Bigg(\frac{\partial \dot{x}^i}{\partial{\dot{q}}^j} \frac{\partial T}{\partial \dot{x}^i}\Bigg) - \frac{\partial T}{\partial \dot{x}^i} \frac{d}{dt}\frac{\partial \dot{x}^i}{\partial \dot{q}^j} = Q_j
\end{equation}

The main difference In Equation (\ref{Eq:LNO}) from the holonomic derivation is the appearance of the total time derivative of $\frac{\partial \dot{x}^i}{\partial \dot{q}^j}$ on the second term of the RHS as opposed to $\frac{\partial \dot{x}^i}{\partial q^j}$ in the holonomic case. We explore the conditions under which these terms would be equal.

Taking the total derivative of $\frac{\partial \dot{x}^i}{\partial \dot{q}^j}$ yields,

\begin{equation} \label{Eq:RHS}
\frac{d}{dt} \frac{\partial \dot{x}^i}{\partial \dot{q}^j} = \frac{d}{dt} h^i_j = \dot{q}^k \frac{\partial h^i_j}{\partial q^k} + \frac{\partial h^i_j}{\partial t}
\end{equation}

On the other hand, partial differentiation of Equation (\ref{Eq:Constraint}) w.r.t the pseudocoordinate $q^k$ yields,

\begin{equation} \label{Eq:OldRHS}
\frac{\partial \dot{x}^i}{\partial q^k} = \dot{q}^j \frac{\partial h^i_j}{\partial q^k} + \frac{\partial h^i_0}{\partial q^k} 
\end{equation}

Comparing Equations (\ref{Eq:RHS}) and (\ref{Eq:OldRHS}), and for homogeneous and affine constraints, we require that
\begin{equation} \label{Eq:IntegrabilityCondition}
\dot{q}^k \frac{\partial h^i_j}{\partial q^k} = \dot{q}^k \frac{\partial h^i_k}{\partial q^j}
\end{equation}

Since pseudovelocities are completely arbitrary, we conclude that Equation (\ref{Eq:IntegrabilityCondition}) is a requirement for $h^i_j$ to be an exact differential of $q^k$. From Frobenus Theroem, this is sufficient for integrabiity, implying that equations (\ref{Eq:Constraint}) are integrable and the system is thus holonomic.

We have just proved that a non-integrable/nonholonomic system cannot be described by Lagrange equations of the second kind. The characterization is exact.

\section{Description of the Problem}
%Should include a figure and the degrees of freedom
\section{Derivation of the Equations}
We use the Lagrangian formalism to derive the dynamics of the double-circular pendulum.

\subsection{Cartesian Coordinates as Function of the Configuration Space}
\begin{equation*}
x = (l_1 \sin(\theta_1) + l_2 \sin(\theta_2))\cos(\phi)
\end{equation*}
\begin{equation*}
y = (l_1 \sin(\theta_1) + l_2 \sin(\theta_2)) \sin(\phi)
\end{equation*}
\begin{equation*}
z = l_1 \cos(\theta_1) + l_2 \cos(\theta_2)
\end{equation*}

\subsection{Derivation of the Lagrangian Function}
The Lagrangian is defined by $L = T - V$ where $T$ and $V$ are the total kinetic and potential energy of the system, respectively.
Since $z$ coordinate points downwards, we derive the potential energy as such.

\begin{equation*}
\boxed{V( \theta_1, \theta_2, \phi) = l_1 + l_2 - z = mg \big( l_1(1 - \cos(\theta_1)) + l_2(1 - \cos(\theta_2)) \big)}
\end{equation*}

We derive the kinetic energy first computing $\dot{x}$, $\dot{y}$ and $\dot{z}$ through differentiation.

\begin{eqnarray*}
& \dot{x} = l_1 \cos(\phi) \cos(\theta_1) \dot{\theta_1} + l_2 \cos(\phi)\cos(\theta_2)\dot{\theta_2} - l_1\sin(\theta_1)\sin(\phi)\dot{\phi} - l_2 \sin(\theta_2) \sin(\phi) \dot{\phi}\\
& \dot{y} = l_1 \sin(\phi) \cos(\theta_1) \dot{\theta_1} + l_2 \sin(\phi)\cos(\theta_2)\dot{\theta_2} + l_1\sin(\theta_1)\cos(\phi)\dot{\phi} + l_2 \sin(\theta_2) \cos(\phi) \dot{\phi}\\
& \dot{z} = -l_1 \sin(\theta_1) \dot{\theta_1} - l_2 \sin(\theta_2) \dot{\theta_2}
\end{eqnarray*}

To compute $T$, we compute $v^2 = \dot{x}^2 + \dot{y}^2 + \dot{z}^2$

\begin{equation*}
T(\theta_1, \theta_2, \phi, \dot{\theta_1}, \dot{\theta_2}, \dot{\phi}) = \frac{1}{2} m v^2 = \boxed{\frac{1}{2} m \big( (l_1 \sin(\theta_1) + l_2 \sin(\theta_2))^2 \dot{\phi}^2 + l_1\dot{\theta_1}^2 + 2 l_1l_2 cos(\theta_1 - \theta_2)\dot{\theta_1}\dot{\theta_2} + l_2 \dot{\theta_2}^2\big)}
\end{equation*}

From which we deduce the Lagrangian of the double-circular pendulum
\begin{equation*}
\boxed{L = \frac{1}{2}m \Big( (l_1 \sin(\theta_1) + l_2 \sin(\theta_2))^2 \dot{\phi}^2 + l_1^2 \dot{\theta_1}^2 + 2 \l_1\l_2 \cos(\theta_1 - \theta_2) \dot{\theta_1}\dot{\theta_2} + l_2^2 \dot{\theta_2}^2 \Big) - mg ( l_1(1 - \cos(\theta_1)) + l_2(1 - \cos(\theta_2)) )}
\end{equation*}

\subsection{Equations of Motion}
Using Euler-Lagrange equations, we derive the system equations of motion for the 3-degrees of freedom system

\begin{eqnarray*}
& \frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial L}{\partial \dot{\phi}} - \frac{\partial L}{\partial \phi} = 0\\
& m\frac{\mathrm{d}}{\mathrm{d}t}(l_1\sin(\theta_1) + l_2 \sin(\theta_2) )\dot{\phi} = 0
\end{eqnarray*}

from which we deduce the conservation of linear momentum in the direction of increasing $\phi$.

\begin{equation*}
\boxed{(l_1\sin(\theta_1) + l_2 \sin(\theta_2)) \dot{\phi} = P_{\phi}},
\end{equation*}
where $P_{\phi}$ is a constant of motion. 

We write down the equations for $\theta_1$ and $\theta_2$. We expect them to look the same due to the symmetry of $L$.

\begin{equation*}
m\frac{\mathrm{d}}{\mathrm{d}t} \Big( l_1^2 \dot{\theta_1} + l_1l_2\cos(\theta_1 - \theta_2) \dot{\theta_2} \Big) = -mg l_1 \sin(\theta_1) + m (l_1\sin(\theta_1) + l_2 \sin(\theta_2))l_1\cos(\theta_1)\dot{\phi}^2 - ml_1l_2sin(\theta_1 - \theta_2)\dot{\theta_1}\dot{\theta_2}
\end{equation*}
%\subsection{A subsection}


\end{document}
