\documentclass[10pt]{article}

\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[cm]{fullpage}

\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}

\title{Local Characterization of Deadlocks and Livelocks on a Directed Graph of Computing Nodes}
\author{Aly Farahat\\
\textit{Intuitive Surgical Inc.}}

\date{}

\begin{document}

\maketitle

\begin{abstract}
In this article, we demonstrate the properties of deadlocks and livelocks on a generic directed graph. We present an algorithm that detects the existence of a deadlock in a graph of finite-state nodes. In addition, we present a characterization of livelocks on the same type of graph. A couple of examples in well known network protocols illustrate our ideas.
\end{abstract}

\section{Introduction}

\section{Defnitions and Model of Computation}
Consider a network as a directed graph $G = (V, E)$ where $V = \{0, \cdots, n-1\}$ is a finite set of \emph{nodes} and $E \subset V \times V$ is a set of arcs between pairs of nodes. For each $i \in V$, there exists an $x_i \in D_i$ where $x_i$ is the variable corresponding to node $i$ and $D_i$ is a finite set/domain from which $x_i$ draws its values. Denote by $\mathcal{N}_i$ the neighborhood of node $i$ which is the set of predecessors of $i$. $\mathcal{N}_i = \{v \in V | (v, i) \in E\}$.
A \emph{local state} $s_i$ of $i$ is a valuation of $x_i$ and of \emph{all} $x_j$ where $j \in \mathcal{N}_i$. The \emph{local state space} $S_i$ of node $i$ is the Cartesian product of the domains of the predecessor variables of $i$ and with its own variable $x_i$; a.k.a, $D_i \times \cdots \times D_j \times \cdots$, where $j \in \mathcal{N}(i)$.  

A local transition $t_i$ is an ordered pair of local states of node $i$ $(s_i, s_i')$; where $s_i$ is its source state and $s_i'$ is its target state. A local transition updates the value of $x_i$ from $s_i$ to $s_i'$. $s_i$ is identical to $s_i'$ except for the value of $x_i$ which gets updated. We say that $t_i$ is \emph{enabled} at $s_i$ \emph{if} the value of $x_i$ holds at $s_i$. We assume that all local transitions are \emph{self-disabling} in that no transition of $i$ is enabled at $s_i'$. An enabled $t_i$ \emph{executes} if it updates the value of $x_i$ from $x_i(s_i)$ to $x_i(s_i')$.  
 

A global state $s$ is a valuation of each $x_i$ for every $i$; i.e., $s \in D_0 \times \cdots D_i \cdots D_{n-1} = S$, where $S$ is the global state space of $G$.

A node $i$ has a \emph{token} if it is in a local state where a local transition is enabled. Since all transitions are self-disabling, an execution of a local transition either passes the token to some of its successors or consumes the token without passing it anywhere. 

A \emph{computation} is a sequence executing local trasitions. The sequence is either inifinte, or it is finite with a terminal global state where no transition is enabled in any node. Since local transitions are all self-disabling, no two consecutive transitions in a computation belong to the same node.

 
$s_j$ is a continuation of $s_i$; $i \in \mathcal{N}_j$ if $s_j$ is an acceptable local state given $s_i$; i.e., $x_j(s_j) = x_j(s_i)$. We denote the continuation binary relation by $\mathcal{C}$. 

It is elementary to see that $\mathcal{C}(s_i, s_i)$; i.e., continuation is reflexive. $\mathcal{C}(s_i, s_j)$ is not symmetric; since our graph is directed. Finally, continuation is \emph{not} transitive. In fact, $\mathcal{C}(s_i, s_j)$ and $\mathcal{C}(s_j, s_k)$ does not necessarily imply that $\mathcal{C}(s_i, s_k)$. Continuation defines a directed bipartite graph on each pair $S_i, S_j$ induced by the topology of $G$.
%Check what type of \emph{relation} compatibility is. Is it an \emph{equivalence}? A partial order? It is neither! It is a \emph{neighborhood} relation. We probably could define a \emph{topology} using the compatibility relation. I am not sure how useful would that be.

\begin{theorem}
A local transition $t_i$ is valid \emph{if and only if} it preserves the continuation graph on $S_i$
\end{theorem}
\begin{proof}
\emph{if}: $t_i$ only writes to $x_i$, $s_i'$ is a continuation of all $\mathcal{N}_i(s_i)$.

\emph{only if}: 
\end{proof}

\begin{definition}
A transition shadow in the local state space of node $j$ is the projection of a transition of some node $i$ in the local state space of node $j$ where $i \in \mathcal{N}_j$.
\end{definition}

The execution of an enabled transition changes the local state of all its successor nodes as dictated by its shadow in each of its successors. In a token passing transition, the target local state of its shadow is a state enabling a local transition in the successor.

\section{Augmented Local Transition Graph}

For each node $i \in V$, an Augmented Local Transition Graph (ALTG) $(S_i, \mathcal{T}_i)$, where $S_i$ is the local state space of node $i$ and $\mathcal{T}_i$ is constructed as follows.

\begin{enumerate}
\item Every local transition $t_i$ is represented by an arc from $s_i$ to $s_i'$ in the graph,
\item every local transition $t_{\mathcal{N}_j}$ has a shadow $(q_i, q_i')$ where, $q_i \in C(s_{\mathcal{N}_j})$ and $q_i' \in C(s'_{\mathcal{N}_j})$. Note that this representation is how node $i$ changes its local state according to the transitions of its predecessors. We represent it by a \emph{dotted} arc in the graph of node $i$.
\end{enumerate} 


\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.7]{figures/Livelocks.eps}
  \caption{ALTG of a General Node in a Uni-ring}
  \label{fig:loclivelocks}
\end{figure}

Figure~\ref{fig:loclivelocks} illustrates potential livelocks occurring due to the shadows of transitions of the predecessor node in a uni-ring.

A computation is thus represented by a sequence of arcs across all ALTGs of the computing nodes. Each global state is a set of local states across which the continuation holds.


\section{Deadlock Characterization}
A deadlock is a global state where no node has a token. In ALTGs, this corresponds to the set $L_i$ of states with no outgoing solid arcs. A continuation relation that holds between states in $L_i$ is a characterization of global deadlocks.

A \emph{local deadlock} $d_i$ is a state with no outgoing transitions or shadows. No matter how transitions excute in any predecessor of $i$, $i$ alwyas remains in $d_i$. Note that a local deadlock of $i$ implies the absence of token from $i$. The converse does not necessarily hold.

\subsection{Deadlocks in Trees}

  
\section{Livelock Characterization}
A protocol on a graph has a livelock if it admits a recurring propagation of tokens through a strongly connected component of its nodes.

Since $p_G$ is finite-state, a livelock has to exactly repeat the same sequence of global states ( the same sequence of local transitions) to construct the livelock.

Our goal is to create equivalence classes of livelocks on graphs in a way similar to livelocks on directed rings. As such, we could use representative livelocks to capture a whole class of livelocks, thereby leveraging partial order reduction.

\subsection{Projection of Livelocks in Local State Space}
A \emph{pseudolivelock} is the projection of a livelock on the local state-space of a node $i$. In the local state-space, local transitions are either \emph{original} or \emph{shadow} transitions. An original transition is a local transition that belongs to process $i$. A shadow transition is the effect of a local transition of a predecessor $j \in \mathcal{N}(i)$ seen in the local state space of process $i$; $i \neq j$.

A livelock is a sequence of local transitions such that original and shadow transitions of $i$ form a pseudolivelock.

\section{Local Reachability of Conjunctive Local Predicates}

\section{Conclusion}

\end{document}